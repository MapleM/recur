#include <iostream>
using namespace std;

int  product(int a, int b)
{
	int result;

	if (a == 0 || b == 0)
	{
		result =  0;
	}
	else if( b > 0 )
	{
		result =  a + product(a, b-1);
	}
	else
	{
		result =  -a + product(a, b+1);
	}
	
	return(result);
}
